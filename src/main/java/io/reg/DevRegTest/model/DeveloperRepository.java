package io.reg.DevRegTest.model;

import io.reg.DevRegTest.Repository.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeveloperRepository extends JpaRepository<Developer,Integer> {
}
