package io.reg.DevRegTest.Repository;


import javax.persistence.*;

@Entity
@Table(name = "developer")
public class Developer {

    @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "DevID")
        private Integer devID;

        @Column(name = "DevName")
        private String devName;

        @Column(name = "DevUsername")
        private String devUsername;

        @Column(name = "devPassword")
        private String devPassword;


    public Developer(String devName, String devUsername, String devPassword) {
        this.devName = devName;
        this.devUsername = devUsername;
        this.devPassword = devPassword;
    }

    public int getDevID() {
        return devID;
    }

    public void setDevID(int devID) {
        this.devID = devID;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String getDevUsername() {
        return devUsername;
    }

    public void setDevUsername(String devUsername) {
        this.devUsername = devUsername;
    }

    public String getDevPassword() {
        return devPassword;
    }

    public void setDevPassword(String devPassword) {
        this.devPassword = devPassword;
    }
}
