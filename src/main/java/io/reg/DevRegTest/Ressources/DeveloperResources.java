package io.reg.DevRegTest.Ressources;


import io.reg.DevRegTest.Repository.Developer;
import io.reg.DevRegTest.model.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/developers")
public class DeveloperResources {

    @Autowired
    DeveloperRepository developerRepository;

    @GetMapping(value = "/all")
    public List<Developer> getAll(){
        return developerRepository.findAll();

    }

    @PostMapping(value = "/load")
    public List<Developer> persist(@RequestBody final Developer developer){

        developerRepository.save(developer);
        return developerRepository.findAll();
    }
}
