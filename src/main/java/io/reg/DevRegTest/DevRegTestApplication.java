package io.reg.DevRegTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "io.reg.DevRegTest.Repository")
@SpringBootApplication
public class DevRegTestApplication {

	public static void main(String[] args) {

		SpringApplication.run(DevRegTestApplication.class, args);
	}

}
